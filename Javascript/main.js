var
    canvas = document.getElementById('background'),
    context = canvas.getContext('2d'),
    score = 0,
    //highscore = document.getElementById('highScore'),
    highscore = 0,


    musicElement = document.getElementById('song'),


    // variables
    alienFrameCounter = 0,
    frameCounter = 0,
    backgroundOffest = 0,
    backgroundVelocity = 10,

    heartFrameCounter = 0,
    heartOffset = 0,

    //images
    background = new Image();
spritesheet = new Image();
alienleft = new Image();
alienright = new Image();


canvas.width = 300;
canvas.height = 700;

//lover
loverSpritePosi = 125;
loverHeight = 400;
loverPrintwidth = 50;
loverPrintheight = 100;

loverLeftClipPosiX = 260;
loverLeftClipPosiY = 0;
loverLeftClipWidth = 228;

loverRightClipPosiX = 0;
loverRightClipPosiY = 403;
loverRightClipWidth = 255;

//heart
heartClipPosiX = 0;
heartClipPosiY = 826;
heartClipWidth = 125;

heartPrintWidth = 50;
heartPrintHeight = 50;

heartSpritePosiX = 150;
heartSpritePosiY = 750;

//alien
alienPrintWidth = 75;
alienPrintHeight = 75;

alienSpritePosiX = 50;
alienSpritePosiY = 750;

alienOffset = 0;


failed = false;
paused = false;
var fps = 0;
var time;
var lasUpdate = 0;


context.fillStyle = "blue";
context.font = "20px Arial ";



function fpsCount() {

    let now = new Date();
    fps = 1000 / (now - lasUpdate);
    lasUpdate = now;

    document.getElementById('FPS').innerHTML = 'FPS: ' + Math.round(fps);


}



function pause() {

    paused = true;

}


function unpause() {

    paused = false;

}


function initializeImages() {

    //creation and settings of the game for the images and such

    if (paused === false) {

        spritesheet.src = "./images/spritesheet.png";
        alienleft.src = "./images/boyoleft.png";
        alienright = ".images/otherboyo.png";

        background.src = "./images/bg.png";
        background.onload = function () {
            startGame();
        }
    }

    else if (paused === true) {
        draw();
    }

}

function startGame() {


    animate();
    startMusic();
}

function draw() {

    //sets the background and base settings mainly in the case of pausing and the initial creation

    drawBackground();
    drawLover();
    drawHeart();

}

function turnLeft() {

    //as paused is a boolean most things only activate if not paused

    if (paused == false && failed == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);

        loverSpritePosi = loverSpritePosi - 10;

        context.drawImage(spritesheet, loverLeftClipPosiX, loverLeftClipPosiY, loverLeftClipWidth, loverHeight, loverSpritePosi, 0, loverPrintwidth, loverPrintheight);

        //doesnt allow the sprite to go off the edge of the screen
        if (loverSpritePosi <= 0) {
            loverSpritePosi = 10;
        }

    }
}

function turnRight() {

    //as paused is a boolean most things only activate if not paused

    if (paused == false && failed == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);

        loverSpritePosi = loverSpritePosi + 10;

        context.drawImage(spritesheet, loverRightClipPosiX, loverRightClipPosiY, loverRightClipWidth, loverHeight, loverSpritePosi, 0, loverPrintwidth + 5, loverPrintheight);


        if (loverSpritePosi >= 235) {
            loverSpritePosi = 230;
        }

    }
}

function drawLover() {

    //changes the character sprite based on its position

    if (loverSpritePosi < 135 && loverSpritePosi >= 120) {
        context.drawImage(spritesheet, 0, 0, 248, loverHeight, loverSpritePosi, 0, loverPrintwidth, loverPrintheight);
    }

    if (loverSpritePosi < 120) {
        context.drawImage(spritesheet, loverLeftClipPosiX, loverLeftClipPosiY, loverLeftClipWidth, loverHeight, loverSpritePosi, 0, loverPrintwidth, loverPrintheight);
    }
    else if (loverSpritePosi >= 135) {
        context.drawImage(spritesheet, loverRightClipPosiX, loverRightClipPosiY, loverRightClipWidth, loverHeight, loverSpritePosi, 0, loverPrintwidth + 5, loverPrintheight);
    }

}


function drawBackground() {

    context.drawImage(background, 0, 0, canvas.width, canvas.height);

}

function speedUp() {
    if (score >= 200) {
        backgroundVelocity = 13;
    }
    if (score >= 500) {
        backgroundVelocity = 17;
    }
    if (score >= 1000) {
        console.log("Why are you still here", backgroundVelocity);
        backgroundVelocity = 20;
    }

    //speeds up background, hearts and alien based on the points obtained in the game

}


function detectHeartTouch() {


    //creates a faux box around the heart and lover sprites that activates function upon collision
    if (loverSpritePosi < heartSpritePosiX + heartPrintWidth &&
        loverSpritePosi + loverPrintwidth > heartSpritePosiX &&
        0 < newHeartSpritePosiY + heartPrintHeight &&
        0 + loverPrintheight >= newHeartSpritePosiY
    ) {

        context.clearRect(heartSpritePosiX, newHeartSpritePosiY, heartPrintWidth, heartPrintHeight);


        context.drawImage(background, 0, backgroundOffest, canvas.width, canvas.height);
        context.drawImage(background, 0, canvas.height + backgroundOffest, canvas.width, canvas.height);

        drawLover();

        //score increases as long as in contact with heart
        score += 1;

        document.getElementById('score').innerHTML = 'Score: ' + score;
        setScore();
    }

    if (score >= highscore) {

        //sets high score
        highscore = score;
        document.getElementById('Highscore').innerHTML = 'Highscore: ' + highscore;
    }
    speedUp();


}


function detectAlienTouch() {

    //same as detectHeartTouch() in esscence
    if (loverSpritePosi < alienSpritePosiX + alienPrintWidth &&
        loverSpritePosi + loverPrintwidth > alienSpritePosiX &&
        0 < newAlienSpritePosiY + heartPrintHeight &&
        0 + loverPrintheight >= newAlienSpritePosiY
    ) {
        gameOver();
    }

}


function drawHeart() {

    context.drawImage(spritesheet, heartClipPosiX, heartClipPosiY, heartClipWidth, heartClipWidth, heartSpritePosiX, heartSpritePosiY, heartPrintWidth, heartPrintHeight);
}


function animate() {

    //all other animarte happen within the background so as to draw over them
    animateBackground();

}

function animateAlien() {
    newAlienSpritePosiY = alienSpritePosiY + alienOffset;



    //alienboy code was initially meant to switch between the 2 kinds of alien sprites based on random calling when first on screen
    // it had a chance to change whenever a new alien appeared onscreen 

    //if (alienboy <= 0.5){

    context.drawImage(alienleft, alienSpritePosiX, newAlienSpritePosiY, alienPrintWidth, alienPrintHeight);

    /*  }
      
      else if(alienboy > 0.5){
          context.drawImage(alienright, alienSpritePosiX, newAlienSpritePosiY, alienPrintWidth, alienPrintHeight);
      }
  */


    alienCount = canvas.height + 100;
    alienFrameCounter = alienFrameCounter + backgroundVelocity;
    alienOffset = alienOffset - (backgroundVelocity + 2);

    if (alienFrameCounter <= alienCount) {
        context.clearRect(0, 0, canvas.width, -canvas.height);
    }


}

function animateHeart() {

    // this and animate alien function similarly with a continuous draw and clear of the  heart, alien and what you will see to be
    // the background; all of these are somewhat relative o the background velocity

    newHeartSpritePosiY = heartSpritePosiY + heartOffset;

    context.drawImage(spritesheet, heartClipPosiX, heartClipPosiY, heartClipWidth, heartClipWidth, heartSpritePosiX, newHeartSpritePosiY, heartPrintWidth, heartPrintHeight);

    heartCount = canvas.height + 100;
    heartFrameCounter = heartFrameCounter + backgroundVelocity;
    heartOffset = heartOffset - backgroundVelocity;


    //deletion of hearts that are off screen
    if (heartFrameCounter <= heartCount) {
        context.clearRect(0, 0, canvas.width, -canvas.height);
    }


}


function gameOver() {


    unpause();
    paused != true;
    failed = true;

    console.log("rip");

    context.clearRect(0, -canvas.height, canvas.width, canvas.height * 3);



    //drwas the objects in all the positions that they are set in within the game over and prints a message over it
    context.drawImage(background, 0, backgroundOffest, canvas.width, canvas.height);

    context.drawImage(background, 0, canvas.height + backgroundOffest, canvas.width, canvas.height);
    drawLover();


    context.drawImage(alienleft, alienSpritePosiX, newAlienSpritePosiY, alienPrintWidth, alienPrintHeight);

    context.drawImage(spritesheet, heartClipPosiX, heartClipPosiY, heartClipWidth, heartClipWidth, heartSpritePosiX, newHeartSpritePosiY, heartPrintWidth, heartPrintHeight);


    context.fillText("Game Over! Press R to retry", 25, canvas.height / 2);
}

function heartRandom() {

    //the random function for the heart giving it a random position within the boundries of the canvas
    if (heartFrameCounter >= heartCount) {
        heartOffset = 0;
        heartFrameCounter = 0;

        heartSpritePosiX = Math.floor((Math.random() * 245) + 5);

        animateHeart();
        heartOffset = heartOffset - backgroundVelocity;
    }

}

function alienRandom() {
    //the same as heartRandom()

    if (alienFrameCounter >= alienCount) {
        alienOffset = 0;
        alienFrameCounter = 0;
        alienSpritePosiX = Math.floor((Math.random() * 245) + 5);
        animateAlien();
        alienOffset = alienOffset - (backgroundVelocity + 2);
    }
}


function animateBackground() {

    fpsCount();

    //freezes the game if paused and just doesnt run 
    if (paused == false && failed == false) {

        context.drawImage(background, 0, backgroundOffest, canvas.width, canvas.height);

        context.drawImage(background, 0, canvas.height + backgroundOffest, canvas.width, canvas.height);


        //alienboy = Math.random();
        animateAlien();
        animateHeart();
        drawLover();


        //randomises the position every iteration of the animate called
        heartRandom();
        alienRandom();

        requestAnimationFrame(animateBackground);

        count = canvas.height;
        frameCounter = frameCounter + backgroundVelocity;
        backgroundOffest = backgroundOffest - backgroundVelocity;

        if (frameCounter >= count) {
            backgroundOffest = 0;
            frameCounter = 0;

        }

        //checks for touch constantly while running
        detectAlienTouch();
        detectHeartTouch();

    }

    else if (paused == true) {

        //gets the current positionof all objects and freezes them in place

        context.drawImage(background, 0, backgroundOffest, canvas.width, canvas.height);

        context.drawImage(background, 0, canvas.height + backgroundOffest, canvas.width, canvas.height);
        drawLover();
        //drawHeart();

        context.drawImage(alienleft, alienSpritePosiX, newAlienSpritePosiY, alienPrintWidth, alienPrintHeight);

        context.drawImage(spritesheet, heartClipPosiX, heartClipPosiY, heartClipWidth, heartClipWidth, heartSpritePosiX, newHeartSpritePosiY, heartPrintWidth, heartPrintHeight);


        context.fillText("Paused", (canvas.width / 2 - 30), canvas.height / 2);

    }


}


function restartGame() {


    //unpauses the game and sets allvalues to zero along with clearing the canvas for the next animate
    unpause();


    context.clearRect(0, -canvas.height, canvas.width, canvas.height * 3);


    score = 0;
    document.getElementById('score').innerHTML = 'Score: ' + score;

    fps = 0;
    lasUpdate = 0;

    frameCounter = 0;
    backgroundOffest = 0;
    backgroundVelocity = 10;

    heartFrameCounter = 0;
    heartOffset = 0;

    alienOffset = 0;
    alienFrameCounter = 0;

}


function startMusic() {
    musicElement.play();
}


//function to store the score in local storage doent appear to work
function setScore() {
    localStorage.setItem("Highscore", highscore);
}

//function to retrieve the score from local storage and place within the highscore vlaue doent appear to work
function getScore() {
    document.getElementById('Highscore').innerHTML = "Highscore: " + localStorage.getItem("Highscore");
}


A_KEY = 65;
LEFT_ARROW = 37;
D_KEY = 68;
RIGHT_ARROW = 39;

P_KEY = 80;
R_KEY = 82;

O_KEY = 79;



//modified from the snailbait code
window.addEventListener('keydown', function (e) {
    var key = e.keyCode;

    if (key === A_KEY || key === LEFT_ARROW) {
        turnLeft();
    }

    else if (key === D_KEY || key === RIGHT_ARROW) {
        turnRight();
    }

    else if (key === P_KEY && failed == false) {
        pause();
    }

    else if (key === O_KEY && failed == false) {

        //set to unpause the gam only if it is alrady paused
        if (paused === true) {
            context.clearRect(0, -canvas.height, canvas.width, canvas.height * 3);
            unpause();
            initializeImages();
        }
    }

    //prevents the game from being restarted while paused mainly to prevent glitching
    else if (key === R_KEY && paused === false) {

        restartGame();

        if (failed == true) {
            failed = false;
            unpause();

            initializeImages();
        }

    }

});

getScore();

//ended up being my start game function in a sense
initializeImages();


/*
Sprite assets taken and edited from the  music video: メルヘル小惑星 / ナナヲアカリ - https://www.youtube.com/watch?v=MGikRKh68gA

Music taken from https://www.youtube.com/watch?v=OcU-TiXybhg&list=LLlS1htxo9AofRzl51Sc9lFw&index=5&t=0s

Assisted with scrolling background by both Aaron Richardson and Tomas Smith.
Assited with fps counter by Aaron Richardson and Andrew Carolan
*/